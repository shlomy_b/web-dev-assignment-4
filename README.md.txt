The Weather class simulates a weather forcasting resource
by displaying the current weather in montreal, as well as
a icon describing the weather. It has two methods;
componentDidMount and render.

componentDidMount gets called once the component 
is rendered. It fetches the API, then checks for 
errors. If there is an error, the method catches 
it and updates the this.state variable, which
decides which div to send to the DOM.

render checks to see if this.state.yesNo is true or false.
It then returns a div specific to the conditions of the
boolean. It also add an onClick to the button in the div
that is returned.



API Used: https://openweathermap.org/weather-conditions