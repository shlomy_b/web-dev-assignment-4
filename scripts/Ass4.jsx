/**
 * The Weather class simulates a weather forcasting resource
 * by displaying the current weather in montreal, as well as
 * a icon describing the weather. 
 *
 * @author: shlomy_b
 * @since 2020-12-11
 */
'use strict';

class Weather extends React.Component {
    constructor(props) {
        super(props); 
        this.tempJson = {};
        this.state = {yesNo: false};
    }

    /**
    * componentDidMount gets called once the component 
    * is rendered. It fetches the API, then checks for 
    * errors. If there is an error, the method catches 
    * it and updates the this.state variable, which
    * decides which div to send to the DOM.
    * 
	*/
    componentDidMount(){
        fetch('http://api.openweathermap.org/data/2.5/weather?q='+(this.props.city)+'&units=metric&appid='+ key)
        .then(response => {
            if (!response.ok) {
                throw new Error('Status code: '  + response.status);
            }
                return response.json();
            })

        .then (json => {
            this.tempJson = json;
            this.setState({yesNo: true}); 
        })
        .catch(error => {
            console.error('There was a problem: '  + error) 
            this.setState({yesNo: false});
        });
    }

    /**
    * render checks to see if this.state.yesNo is true or false.
    * It then returns a div specific to the conditions of the
    * boolean. It also add an onClick to the button in the div
    * that is returned. 
    * 
    * */
    render() {
        if(this.state.yesNo){
            return(
            <div>
            <p>{this.tempJson.main.temp} °C {this.tempJson.weather[0].description}</p>
            <img src={'http://openweathermap.org/img/wn/' + this.tempJson.weather[0].icon + '.png'}></img> 
            <button onClick = {() => this.componentDidMount()} >Refresh</button>
        </div> 
            )
        }else{
            return(
                 <div>
                     <p>Weather Is Not Available At This Time</p>
                     <button onClick = {() => this.componentDidMount()} >Refresh</button>
                 </div> 
             )
        }
    }
}
    // key constant
    const key = '3834a9eb3e1bddf6b768815082c64329';

    /*  This stores the location of the container that the 
    *   divs are appended to.  */
    const domContainer = document.querySelector('#container');

    /*  This renders the divs to the container.  */
    ReactDOM.render(<Weather city='montreal'/>, domContainer);