"use strict";

function WeatherTab() {
    return React.createElement( 
        'div',
        {style: {border: "10px, solid", background: "yellow"}},
        "this is a test"
    );
}

function Heading3(props) {
    return React.createElement( 
        'h1', {
            style: { background: props.colour }
        },
        props.message
    );
}


document.addEventListener('DOMContentLoaded', setup);

// my key 3834a9eb3e1bddf6b768815082c64329
// the get https://samples.openweathermap.org/data/2.5/weather?q=Montreal&appid=3834a9eb3e1bddf6b768815082c64329
// or https://api.openweathermap.org/data/2.5/weather?q={enter city prop here}&appid=3834a9eb3e1bddf6b768815082c64329

https://cors-anywhere.herokuapp.com/http://api.openweathermap.org...



function setup() {
    const domContainer = document.querySelector('#container');
    ReactDOM.render(React.createElement(WeatherTab), domContainer);
    ReactDOM.render(React.createElement(Heading3, { colour: 'lightblue', message: 'properties' }), domContainer);
}